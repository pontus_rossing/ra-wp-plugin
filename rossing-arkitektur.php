<?php
/**
 * Plugin Name:       Rossing Arkitektur
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           0.0.3
 * Author:            PR
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

//Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Vår organisation

function ra_register_organisationsegenskap() {
	
	$singular = 'Vår organisation';
	$plural = 'Vår organisation';
	$slug = str_replace( ' ', '_', strtolower( $singular ) );
	$labels = array(
		'name' 			=> $plural,
		'singular_name' 	=> $singular,
		'add_new' 		=> 'Lägg till',
		'add_new_item'  	=> 'Lägg till ' . $singular,
		'edit'		        => 'Redigera',
		'edit_item'	        => 'Redigera ' . $singular,
		'new_item'	        => 'Ny ' . $singular,
		'view' 			=> 'Visa ' . $singular,
		'view_item' 		=> 'View ' . $singular,
		'search_term'   	=> 'Sök ' . $plural,
		'parent' 		=> 'Förälder ' . $singular,
		'not_found' 		=> 'Ingen ' . $plural .' hittad',
		'not_found_in_trash' 	=> 'Ingen ' . $plural .' i papperskorgen'
		);
	$args = array(
		'labels'              => $labels,
	        'public'              => true,
	        'publicly_queryable'  => true,
	        'exclude_from_search' => false,
	        'show_in_nav_menus'   => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 10,
	        'menu_icon'           => 'dashicons-lightbulb',
	        'can_export'          => true,
	        'delete_with_user'    => false,
	        'hierarchical'        => false,
	        'has_archive'         => true,
	        'query_var'           => true,
	        'capability_type'     => 'post',
	        'map_meta_cap'        => true,
	        // 'capabilities' => array(),
	        'rewrite'             => array( 
	        	'slug' => $slug,
	        	'with_front' => true,
	        	'pages' => true,
	        	'feeds' => true,
	        ),
	        'supports'            => array( 
	        	'title',
	        	'excerpt', 
	        	'editor',  
	        	'thumbnail',
	        	'revisions' 
	        )
	);
	register_post_type( $slug, $args );
}
add_action( 'init', 'ra_register_organisationsegenskap' );



// Om Henriksberg

function ra_register_henriksberg() {
	
	$singular = 'Om Henriksberg';
	$plural = 'Om Henriksberg';
	$slug = str_replace( ' ', '_', strtolower( $singular ) );
	$labels = array(
		'name' 			=> $plural,
		'singular_name' 	=> $singular,
		'add_new' 		=> 'Lägg till',
		'add_new_item'  	=> 'Lägg till ' . $singular,
		'edit'		        => 'Redigera',
		'edit_item'	        => 'Redigera ' . $singular,
		'new_item'	        => 'Ny ' . $singular,
		'view' 			=> 'Visa ' . $singular,
		'view_item' 		=> 'View ' . $singular,
		'search_term'   	=> 'Sök ' . $plural,
		'parent' 		=> 'Förälder ' . $singular,
		'not_found' 		=> 'Ingen ' . $plural .' hittad',
		'not_found_in_trash' 	=> 'Ingen ' . $plural .' i papperskorgen'
		);
	$args = array(
		'labels'              => $labels,
	        'public'              => true,
	        'publicly_queryable'  => true,
	        'exclude_from_search' => false,
	        'show_in_nav_menus'   => true,
	        'show_ui'             => true,
	        'show_in_menu'        => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 10,
	        'menu_icon'           => 'dashicons-admin-multisite',
	        'can_export'          => true,
	        'delete_with_user'    => false,
	        'hierarchical'        => false,
	        'has_archive'         => true,
	        'query_var'           => true,
	        'capability_type'     => 'post',
	        'map_meta_cap'        => true,
	        // 'capabilities' => array(),
	        'rewrite'             => array( 
	        	'slug' => $slug,
	        	'with_front' => true,
	        	'pages' => true,
	        	'feeds' => true,
	        ),
	        'supports'            => array( 
	        	'title',
	        	'excerpt', 
	        	'editor',  
	        	'thumbnail',
	        	'revisions' 
	        )
	);
	register_post_type( $slug, $args );
}
add_action( 'init', 'ra_register_henriksberg' );


// Custom fields

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_jumbotron',
		'title' => 'Jumbotron',
		'fields' => array (
			array (
				'key' => 'field_59033a9e81aac',
				'label' => 'Jumbotron heading',
				'name' => 'jumbotron_heading',
				'type' => 'text',
				'instructions' => 'Text för huvudrubrik på jumbotron.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_59033fe68e325',
				'label' => 'Jumbotron subheading',
				'name' => 'jumbotron_subheading',
				'type' => 'text',
				'instructions' => 'Text under rubrik',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_59034594259c0',
				'label' => 'Jumbotron knapp',
				'name' => 'jumbotron_knapp',
				'type' => 'true_false',
				'instructions' => 'Ska det finnas en länkknapp?',
				'message' => 'Ska det finnas en länknapp?',
				'default_value' => 1,
			),
			array (
				'key' => 'field_590340588e326',
				'label' => 'Jumbotron knapplänk',
				'name' => 'jumbotron_knapplank',
				'type' => 'page_link',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_59034594259c0',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'post_type' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_590346383b373',
				'label' => 'Jumbotron knapptext',
				'name' => 'jumbotron_knapptext',
				'type' => 'text',
				'instructions' => 'Vad ska det stå på knappen?',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_59034594259c0',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'default_value' => 'Läs mer',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}









